/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/data/drink_list_provider.dart';
import 'package:drink_manager/data/drink_provider.dart';
import 'package:drink_manager/data/list_drink_join_provider.dart';
import 'package:drink_manager/main.dart';
import 'package:drink_manager/pages/home.dart';
import 'package:drink_manager/widgets/drink_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class MyAppProxy extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyAppProxy> {
  Database db;

  DrinkProvider drinkProvider = DrinkProvider.get();
  DrinkListProvider listProvider = DrinkListProvider.get();
  ListDrinkJoinProvider listDrinkJoinProvider = ListDrinkJoinProvider.get();

  StreamSubscription<ListChangedEvent> stream;

  @override
  void initState() {
    super.initState();

    CacheManager.maxNrOfCacheObjects = double.maxFinite.floor();
    CacheManager.maxAgeCacheObject = Duration(days: double.maxFinite.floor());

    stream = MyApp.bus.on<ListChangedEvent>().listen((event) {
      if (event.change == ListChange.Added || event.change == ListChange.Set)
        ListDrinkJoinProvider.get()
            .insertAllDrink(event.shoppingList, event.drinks);
      else if (event.change == ListChange.Removed)
        ListDrinkJoinProvider.get()
            .deleteAllDrink(event.shoppingList, event.drinks);
    });

    _init();
  }

  Future _init() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, "drinks.db");

    db = await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await drinkProvider.onCreate(db, version);
        await listProvider.onCreate(db, version);
        await listDrinkJoinProvider.onCreate(db, version);
      },
    );

    drinkProvider.open(db);
    listProvider.open(db);
    listDrinkJoinProvider.open(db);

    MyApp.isDatabaseOpen = true;
    MyApp.bus.fire(DatabaseOpenedEvent());
  }

  @override
  void dispose() {
    db.close();
    stream.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Drink Manager',
      theme: ThemeData(
          primarySwatch: Colors.red, accentColor: Colors.deepPurpleAccent),
      home: new MyHomePage(title: 'Drink Manager'),
    );
  }
}

class DatabaseOpenedEvent {}
