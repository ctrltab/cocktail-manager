/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'package:cached_network_image/cached_network_image.dart';
import 'package:drink_manager/data/drink_provider.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/widgets/drink_preview.dart';
import 'package:drink_manager/widgets/ingredient_preview.dart';
import 'package:drink_manager/widgets/shopping_list_dialog.dart';
import 'package:flutter/material.dart';

class DrinkShowcase extends StatefulWidget {
  final Drink drink;

  DrinkShowcase(this.drink);

  static showDrink(Drink c, context) {
    Navigator.of(context).push(
      new MaterialPageRoute(
        builder: (BuildContext context) {
          return DrinkShowcase(c);
        },
      ),
    );
  }

  @override
  State<StatefulWidget> createState() => new _DrinkShowcaseState();

  static SliverAppBar _getAppBar(
      BuildContext context, Drink c, bool innerBoxIsScrolled) {
    return SliverAppBar(
      primary: true,
      pinned: true,
      forceElevated: innerBoxIsScrolled,
      expandedHeight: 400.0,
      actions: <Widget>[
        new DrinkFav(c),
        IconButton(
            icon: Icon(
              Icons.fullscreen,
            ),
            onPressed: () {}),
        new PopupMenuButton<String>(
          itemBuilder: (_) => [
                new PopupMenuItem(
                  child: new Text('Add to Shoping List'),
                  value: 'list',
                )
              ],
          onSelected: (val) {
            if (val == 'list') {
              showDialog(
                  context: context,
                  builder: (context) => AddToDrinkListDialog(c));
            }
          },
        ),
      ],
      flexibleSpace: new FlexibleSpaceBar(
        title: Text(
          c.strDrink,
          maxLines: 1,
        ),
        background: Hero(
          tag: 'image:${c.idDrink}',
          child: new Stack(
            children: <Widget>[
              new Positioned.fill(
                child: new DecoratedBox(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      fit: BoxFit.cover,
                      image: new CachedNetworkImageProvider(c.strDrinkThumb),
                    ),
                  ),
                ),
              ),
              new Positioned.fill(
                child: new DecoratedBox(
                  decoration: new BoxDecoration(
                    gradient: const LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: [0.0, 0.2, 0.7, 1.0],
                      colors: const <Color>[
                        const Color(0x80000000),
                        const Color(0x00000000),
                        const Color(0x00000000),
                        const Color(0x80000000)
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _DrinkShowcaseState extends State<DrinkShowcase> {
  @override
  Widget build(BuildContext context) {
    DrinkInfo info = DrinkInfo(widget.drink);
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          DrinkShowcase._getAppBar(context, widget.drink, false),
          SliverToBoxAdapter(
            child: info,
          ),
        ],
      ),
    );
  }
}

class DrinkInfo extends StatefulWidget {
  final Drink c;

  DrinkInfo(this.c);

  @override
  State<StatefulWidget> createState() => _DrinkInfoState();
}

class _DrinkInfoState extends State<DrinkInfo> {
  int expanded = -1;

  @override
  Widget build(BuildContext context) {
    var children = <Widget>[];

    var panels = ExpansionPanelList(
      children: [
        ExpansionPanel(
            headerBuilder: (context, isExpanded1) {
              return Padding(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Info'),
                    Builder(
                      builder: (context) {
                        if (expanded != 0) {
                          return Text(
                            widget.c.strDrink,
                            style:
                                TextStyle(color: Theme.of(context).hintColor),
                          );
                        } else {
                          return Text('');
                        }
                      },
                    ),
                  ],
                ),
                padding: EdgeInsets.all(16.0),
              );
            },
            body: Padding(
              padding: EdgeInsets.all(4.0),
              child: Builder(
                builder: (context) {
                  var infoChildren = <TableRow>[
                    TableRow(
                        children: [Text('Glass: '), Text(widget.c.strGlass)]),
                    TableRow(children: [
                      Text('Category: '),
                      Text(widget.c.strCategory)
                    ]),
                  ];

                  if (widget.c.strIBA.isNotEmpty)
                    infoChildren.add(
                      TableRow(
                          children: [Text('IBA: '), Text(widget.c.strIBA)]),
                    );
                  if (widget.c.strAlcoholic != null &&
                      widget.c.strAlcoholic.isNotEmpty)
                    infoChildren.add(
                      TableRow(children: [
                        Text('Alcoholic: '),
                        Text(widget.c.strAlcoholic)
                      ]),
                    );

                  return Column(
                    children: <Widget>[
                      Text(
                        widget.c.strDrink,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Table(
                          columnWidths: {
                            0: IntrinsicColumnWidth(),
                            1: IntrinsicColumnWidth(),
                          },
                          children: infoChildren,
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
            isExpanded: expanded == 0),
        ExpansionPanel(
            headerBuilder: (context, isExpanded1) {
              return Padding(
                child: Text('Instructions'),
                padding: EdgeInsets.all(16.0),
              );
            },
            body: Padding(
              padding: EdgeInsets.all(4.0),
              child: Center(child: Text(widget.c.strInstructions)),
            ),
            isExpanded: expanded == 1),
      ],
      expansionCallback: (i, b) {
        setState(() {
          if (!b)
            expanded = i;
          else
            expanded = -1;
        });
      },
    );

    children.add(panels);

    children.add(
      Card(
        margin: EdgeInsets.symmetric(vertical: 8.0),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 4.0),
          child: Column(
            children: widget.c.ingredients.entries.map((entry) {
              /*return IngredientPreview(
                entry.key,
                trailing: Text(
                  entry.value,
                  style: TextStyle(color: Theme.of(context).hintColor),
                ),
              );*/

              return ListTile(
                dense: true,
                onTap: () {
                  showModalBottomSheet(
                    context: context,
                    builder: (context) => BottomSheet(
                          onClosing: () {},
                          builder: (context) => Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text('Recepies with ${entry.key}'),
                                  Container(
                                    height: 200.0,
                                    child: FutureBuilder<List<Drink>>(
                                      future: DrinkProvider.get()
                                          .getDrinksForIngredient(entry.key),
                                      builder: (builder, snapshot) {
                                        if (snapshot.hasData) {
                                          return SingleChildScrollView(
                                            child: Column(
                                              children: snapshot.data
                                                  .map((c) => DrinkPreview(c))
                                                  .toList(),
                                            ),
                                          );
                                        } else {
                                          return CircularProgressIndicator();
                                        }
                                      },
                                    ),
                                  )
                                ],
                              ),
                        ),
                  );
                },
                leading: CachedNetworkImage(
                  imageUrl: 'https://www.thecocktaildb.com/images/ingredients/${entry.key}.png',
                  width: 100.0,
                  height: 56.0,
                  fit: BoxFit.fitHeight,
                ),
                title: Text(
                  entry.key,
                ),
                trailing: Text(
                  entry.value,
                  style: TextStyle(color: Theme.of(context).hintColor),
                ),
              );
            }).toList(),
          ),
        ),
      ),
    );

    return new SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: children,
        ),
      ),
    );
  }
}
