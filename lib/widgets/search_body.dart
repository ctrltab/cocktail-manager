/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:flutter/material.dart';

typedef String GetValue<T>(T element);

typedef Widget WidgetCreator<T>(T elem);

class SearchBody<T> extends StatefulWidget {
  final Future<List<T>> list;
  final WidgetCreator<dynamic> builder;
  final GetValue<dynamic> getValue;
  final String title;

  SearchBody({
    @required this.list,
    @required this.getValue,
    @required this.builder,
    this.title: '',
    Key key,
  }) : super(key: key);

  @override
  _SearchBodyState<T> createState() => new _SearchBodyState<T>();
}

class _SearchBodyState<T> extends State<SearchBody>
    with SingleTickerProviderStateMixin {
  TextEditingController _controller;
  var _listener;
  bool isSearchFieldVisible = false;
  List<T> list;
  List<T> showing = new List();

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController();
    _listener = () {
      setState(
        () {
          showing = list
              .where((str) => widget
                  .getValue(str)
                  .toLowerCase()
                  .contains(_controller.text.toLowerCase()))
              .toList();
        },
      );
    };
    _controller.addListener(_listener);
    widget.list.then(
      (e) => this.setState(
            () {
              list = e;
              showing = list;
            },
          ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PreferredSizeWidget appBar;
    if (isSearchFieldVisible) {
      appBar = AppBar(
        key: ValueKey('seachappbar'),
        backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: Theme.of(context).iconTheme,
        title: new TextField(
          autofocus: true,
          maxLines: 1,
          controller: _controller,
          decoration: InputDecoration.collapsed(
              hintText: 'Search', border: UnderlineInputBorder()),
        ),
      );
    } else {
      appBar = AppBar(
        key: ValueKey('normalappbar'),
        title: new Text(widget.title),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.search),
            onPressed: () {
              setState(() {
                {
                  ModalRoute.of(context).addLocalHistoryEntry(
                      LocalHistoryEntry(onRemove: () {
                        setState(() {
                          isSearchFieldVisible = false;
                        });
                      }));

                  setState(() {
                    isSearchFieldVisible = true;
                  });
                  // searching = true;
                }
              });
            },
          )
        ],
      );
    }

    appBar = PreferredSize(
      preferredSize: appBar.preferredSize,
      child: AnimatedSwitcher(
        child: appBar,
        duration: Duration(milliseconds: 200),
        transitionBuilder: (child, animataion) {
          return FadeTransition(
            opacity: animataion,
            child: child,
          );
        },
      ),
    );

    Widget body;

    if (list == null) {
      body = new LinearProgressIndicator();
    } else {
      /*body = new OverviewWidget<T>(
        elements: showing,
        builder: widget.builder,
        onSelect: widget.onSelect,
        onLongPress: widget.onLongPress,
      );*/
      body = ListView.separated(
        primary: true,
        padding: EdgeInsets.symmetric(vertical: 8.0),
        itemCount: showing.length,
        addAutomaticKeepAlives: false,
        addRepaintBoundaries: false,
        itemBuilder: (context, index) => widget.builder(showing[index]),
        separatorBuilder: (BuildContext context, int index) => Divider(
              height: 0.0,
              indent: 0.0,
            ),
      );
    }

    return new Scaffold(appBar: appBar, body: body);
  }
}
