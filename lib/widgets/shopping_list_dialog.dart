/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'package:drink_manager/data/drink_list_provider.dart';
import 'package:drink_manager/data/list_drink_join_provider.dart';
import 'package:drink_manager/main.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/shopping_list.dart';
import 'package:drink_manager/widgets/drink_preview.dart';
import 'package:flutter/material.dart';

class AddToDrinkListDialog extends StatefulWidget {
  final Drink c;

  AddToDrinkListDialog(this.c);

  @override
  State<StatefulWidget> createState() => _AddToDrinkListDialogState();
}

class _AddToDrinkListDialogState extends State<AddToDrinkListDialog> {
  Map<ShoppingList, bool> list = {};

  @override
  void initState() {
    DrinkListProvider.get().getShoppingLists().then((lists) {
      lists.forEach((shop) {
        ListDrinkJoinProvider.get().getDrinksForList(shop).then((drinks) {
          this.list[shop] = drinks.contains(widget.c);
          if (this.list.keys.length == lists.length) {
            setState(() {});
          }
        });
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Select Shopping List'),
      contentPadding: EdgeInsets.only(top: 12.0),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text('Ok'),
        ),
      ],
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: ListView(
              padding: EdgeInsets.only(),
              primary: true,
              shrinkWrap: true,
              children: list.entries
                  .map<Widget>(
                    (f) => ListTile(
                          onTap: () {
                            setState(() {
                              list[f.key] = !f.value;
                            });
                            MyApp.bus.fire(ListChangedEvent(
                                change: list[f.key]
                                    ? ListChange.Added
                                    : ListChange.Removed,
                                drinks: Set.of([widget.c]),
                                shoppingList: f.key));
                          },
                          leading: Checkbox(value: f.value, onChanged: null),
                          title: Text(
                            f.key.title,
                          ),
                        ),
                  )
                  .toList(),
            ),
          ),
          Divider(
            height: 0.0,
          ),
          ListTile(
            leading: Icon(Icons.add),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) => NewShoppingListDialog()).then((res) {
                if (res != null) {
                  setState(() {
                    list[res] = false;
                  });
                }
              });
            },
            title: new Text(
              'New Shopping List',
            ),
          ),
          Divider(
            height: 0.0,
          )
        ],
      ),
    );
  }
}

class NewShoppingListDialog extends StatelessWidget {
  final TextEditingController field = TextEditingController();

  NewShoppingListDialog();

  @override
  Widget build(BuildContext context) {
    void onOk(String s) async{
      ShoppingList l;
      if (field.text.isNotEmpty) {
        l = ShoppingList(field.text);
        await DrinkListProvider.get().insert(l);
        MyApp.bus.fire(ListChangedEvent(
            change: ListChange.Set, drinks: Set<Drink>(), shoppingList: l));
      }
      Navigator.of(context).pop(l);
    }

    return AlertDialog(
      title: Text('New List'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: field,
            maxLines: 1,
            autocorrect: true,
            autofocus: true,
            onSubmitted: (_) {
              //TODO: Change to next TextField
            },
          ),
          TextField(
            decoration: InputDecoration(
                labelText: 'Description', hintText: '(Optional)'),
            onSubmitted: onOk,
          )
        ],
      ),
      actions: [
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text('Cancel'),
        ),
        FlatButton(
          onPressed: () => onOk(''),
          child: Text('Ok'),
        ),
      ],
    );
  }
}
