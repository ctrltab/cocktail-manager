/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:drink_manager/data/list_drink_join_provider.dart';
import 'package:drink_manager/main.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/shopping_list.dart';
import 'package:drink_manager/widgets/drink_showcase.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DrinkPreview extends StatefulWidget {
  final Drink drink;
  final GestureTapCallback onTap;
  final GestureLongPressCallback onLongPress;
  final bool selected;

  //TODO: Maybe put key in the constructor
  DrinkPreview(this.drink,
      {this.onTap, this.onLongPress, this.selected = false})
      : super(key: Key("preview:${drink.idDrink}"));

  @override
  _DrinkPreviewState createState() => new _DrinkPreviewState();
}

class _DrinkPreviewState extends State<DrinkPreview> {
  @override
  Widget build(BuildContext context) {
    Widget ingredients;
    if (widget.drink.ingredients.isEmpty) {
      ingredients = new Text('');
    } else {
      ingredients = new Text(
        widget.drink.ingredients.keys.join(", "),
        maxLines: 2,
      );
    }
    return ListTile(
      isThreeLine: true,
      contentPadding: EdgeInsets.only(),
      selected: widget.selected,
      leading: widget.selected
          ? Stack(
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: widget.drink.strDrinkThumb,
                  width: 100.0,
                  height: 56.0,
                  fit: BoxFit.fitWidth,
                ),
                Positioned.fill(
                    child: Container(
                  color: Theme.of(context).cardColor.withAlpha(170),
                  child: Icon(
                    Icons.check,
                    color: Colors.green,
                    size: 42.0,
                  ),
                )),
              ],
            )
          : Hero(
              tag: 'image:${widget.drink.idDrink}',
              child: CachedNetworkImage(
                imageUrl: widget.drink.strDrinkThumb,
                width: 100.0,
                height: 56.0,
                fit: BoxFit.fitWidth,
              ),
            ),
      trailing: DrinkFav(widget.drink),
      title: Text(
        widget.drink.strDrink,
        style: TextStyle(fontWeight: FontWeight.w600),
        maxLines: 1,
        softWrap: true,
      ),
      subtitle: ingredients,
      onTap: widget.onTap ??
          () {
            DrinkShowcase.showDrink(widget.drink, context);
          },
      onLongPress: widget.onLongPress,
    );
  }
}

class DrinkFav extends StatefulWidget {
  final Drink drink;

  DrinkFav(this.drink);

  @override
  _DrinkFavState createState() => _DrinkFavState();
}

class _DrinkFavState extends State<DrinkFav> {
  bool _fav = false;

  StreamSubscription<ListChangedEvent> stream;

  @override
  void initState() {
    super.initState();

    ListDrinkJoinProvider.get()
        .contains(ShoppingLists.fav, widget.drink.idDrink)
        .then((b) {
      setState(() {
        _fav = b;
      });
    });

    stream = MyApp.bus.on<ListChangedEvent>().listen((event) {
      if (event.shoppingList == ShoppingLists.fav &&
          event.drinks.contains(widget.drink)) {
        setState(() {
          _fav =
              event.change == ListChange.Added || event.change == ListChange.Set
                  ? true
                  : false;
        });
      }
    });
  }

  @override
  void dispose() {
    stream.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Hero(
        tag: 'fav:${widget.drink.idDrink}',
        child: Icon(
          _fav ? Icons.favorite : Icons.favorite_border,
          color: Theme.of(context).accentColor,
        ),
      ),
      onPressed: () {
        MyApp.bus.fire(ListChangedEvent(
            change: _fav ? ListChange.Removed : ListChange.Added,
            drinks: Set.of([widget.drink]),
            shoppingList: ShoppingLists.fav));
      },
    );
  }
}

enum ListChange { Added, Removed, Set }

class ListChangedEvent {
  final ListChange change;
  final Set<Drink> drinks;
  final ShoppingList shoppingList;

  ListChangedEvent(
      {@required this.change,
      @required this.drinks,
      @required this.shoppingList});
}
