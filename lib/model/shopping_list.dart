/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'package:quiver/core.dart';

String tableList = 'list';
String tableListDrinkJoin = 'list_drink_join';

final String columnIdList = '_id';
final String columnStrTitle = 'strTitle';
final String columnStrDescription = 'strDescription';

final String columnJoinListId = 'id_list';
final String columnJoinDrinkId = 'id_drink';

class ShoppingList {
  int id;
  String title;
  String description;

  ShoppingList(this.title, {this.description = '', this.id});

  Map<String, dynamic> toMap() {
    return {
      columnIdList: id,
      columnStrTitle: title,
      columnStrDescription: description
    };
  }

  factory ShoppingList.fromMap(Map map) {
    return ShoppingList(map[columnStrTitle],
        id: map[columnIdList], description: map[columnStrDescription]);
  }

  @override
  bool operator ==(other) {
    if (other is ShoppingList)
      return id == other.id;
    else
      return false;
  }

  @override
  int get hashCode => hashObjects([id]);
}

class ShoppingLists {
  static final ShoppingList fav = ShoppingList('Favorites', id: 0);
}
