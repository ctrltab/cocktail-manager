/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'package:quiver/core.dart';

final String tableDrink = 'drinks';

final String columnIdDrink = 'idDrink';
final String columnStrDrink = 'strDrink';
final String columnStrVideo = 'strVideo';
final String columnStrCategory = 'strCategory';
final String columnStrIBA = 'strIBA';
final String columnStrAlcoholic = 'strAlcoholic';
final String columnStrGlass = 'strGlass';
final String columnStrInstructions = 'strInstructions';
final String columnStrDrinkThumb = 'strDrinkThumb';
final String columnStrIngredient1 = 'strIngredient1';
final String columnStrIngredient2 = 'strIngredient2';
final String columnStrIngredient3 = 'strIngredient3';
final String columnStrIngredient4 = 'strIngredient4';
final String columnStrIngredient5 = 'strIngredient5';
final String columnStrIngredient6 = 'strIngredient6';
final String columnStrIngredient7 = 'strIngredient7';
final String columnStrIngredient8 = 'strIngredient8';
final String columnStrIngredient9 = 'strIngredient9';
final String columnStrIngredient10 = 'strIngredient10';
final String columnStrIngredient11 = 'strIngredient11';
final String columnStrIngredient12 = 'strIngredient12';
final String columnStrIngredient13 = 'strIngredient13';
final String columnStrIngredient14 = 'strIngredient14';
final String columnStrIngredient15 = 'strIngredient15';
final String columnStrMeasure1 = 'strMeasure1';
final String columnStrMeasure2 = 'strMeasure2';
final String columnStrMeasure3 = 'strMeasure3';
final String columnStrMeasure4 = 'strMeasure4';
final String columnStrMeasure5 = 'strMeasure5';
final String columnStrMeasure6 = 'strMeasure6';
final String columnStrMeasure7 = 'strMeasure7';
final String columnStrMeasure8 = 'strMeasure8';
final String columnStrMeasure9 = 'strMeasure9';
final String columnStrMeasure10 = 'strMeasure10';
final String columnStrMeasure11 = 'strMeasure11';
final String columnStrMeasure12 = 'strMeasure12';
final String columnStrMeasure13 = 'strMeasure13';
final String columnStrMeasure14 = 'strMeasure14';
final String columnStrMeasure15 = 'strMeasure15';
final String columnDateModified = 'dateModified';

class Drink implements Comparable<Drink>{
  int idDrink;
  String strDrink;
  String strVideo;
  String strCategory;
  String strIBA;
  String strAlcoholic;
  String strGlass;
  String strInstructions;
  String strDrinkThumb;
  Map<String, String> ingredients;
  String dateModified;

  Drink(
      {this.idDrink,
      this.strDrink,
      this.strVideo,
      this.strCategory,
      this.strIBA,
      this.strAlcoholic,
      this.strGlass,
      this.strInstructions,
      this.strDrinkThumb,
      this.ingredients,
      this.dateModified});

  @override
  bool operator ==(other) {
    if (other is Drink)
      return idDrink == other.idDrink;
    else
      return false;
  }

  @override
  int get hashCode => hashObjects([idDrink]);

  factory Drink.fromJson(Map<String, dynamic> json) {
    Map<String, String> map = new Map();
    for (int i = 1; i <= 15; i++) {
      if (json['strIngredient$i'] == ' ' ||
          (json['strIngredient$i'] == '') ||
          json['strIngredient$i'] == null) break;
      map[json['strIngredient$i']] = (json['strMeasure$i'] ??= '');
      map[json['strIngredient$i']].replaceAll('\n', '');
    }

    return new Drink(
      idDrink: int.parse(json['idDrink']),
      strDrink: json['strDrink'],
      strCategory: json['strCategory'] ??= "",
      strVideo: json['strVideo'] ??= "",
      strIBA: json['strIBA'] ??= "",
      strAlcoholic: json['alcoholic'],
      strGlass: json['strGlass'] ??= "",
      strInstructions: json['strInstructions'] ??= "",
      strDrinkThumb: json['strDrinkThumb'] ??= "",
      ingredients: map,
      dateModified: json['dateModified'] ??= "",
    );
  }

  factory Drink.fromMap(Map map) {
    Map<String, String> ing = new Map();
    for (int i = 1; i <= 15; i++) {
      if (map['strIngredient$i'] == ' ' ||
          (map['strIngredient$i'] == '') ||
          map['strIngredient$i'] == null) break;
      ing[map['strIngredient$i']] = map['strMeasure$i'] ?? "";
      ing[map['strIngredient$i']].replaceAll('\n', '');
    }

    return new Drink(
      idDrink: map[columnIdDrink],
      strDrink: map[columnStrDrink],
      strVideo: map[columnStrVideo] ?? "",
      strCategory: map[columnStrCategory] ?? "",
      strIBA: map[columnStrIBA] ?? "",
      strAlcoholic: map[columnStrAlcoholic],
      strGlass: map[columnStrGlass] ?? "",
      strInstructions: map[columnStrInstructions] ?? "",
      strDrinkThumb: map[columnStrDrinkThumb] ?? "",
      ingredients: ing,
      dateModified: map[columnDateModified] ?? "",
    );
  }

  @override
  int compareTo(Drink other) {
    return this.strDrink.compareTo(other.strDrink);
  }
}
