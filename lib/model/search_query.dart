/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/model/drink.dart';
import 'package:quiver/core.dart';

typedef Future<List<Drink>> GetDrinks(String query);
typedef Future<List<String>> GetQueries();

class SearchQuery {
  final String name;
  final GetDrinks drinks;
  final GetQueries queries;

  SearchQuery(this.name, this.drinks, this.queries);

  Future<List<Drink>> getDrinks(String query) {
    return drinks(query);
  }

  Future<List<String>> getQueries() async {
    List<String> list = await queries();
    list.remove(null);
    return list;
  }

  @override
  bool operator ==(other) {
    if (other is SearchQuery)
      return name == other.name;
    else
      return false;
  }

  @override
  int get hashCode => hashObjects([name]);
}
