/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/shopping_list.dart';
import 'package:sqflite/sqflite.dart';

class ListDrinkJoinProvider {
  static final ListDrinkJoinProvider _provider =
      new ListDrinkJoinProvider._internal();

  ListDrinkJoinProvider._internal();

  static ListDrinkJoinProvider get() => _provider;

  Database db;

  void open(Database db) {
    this.db = db;
  }

  Future onCreate(Database db, int version) {
    return db.execute('''
     create table $tableListDrinkJoin (
     $columnJoinListId integer,
     $columnJoinDrinkId integer,
     PRIMARY KEY ($columnJoinListId, $columnJoinDrinkId),
     FOREIGN KEY($columnJoinListId) REFERENCES $tableList($columnIdList),
     FOREIGN KEY($columnJoinDrinkId) REFERENCES $tableDrink($columnIdDrink))''');
  }

  Future insert(ShoppingList list, int drinkId) async {
    await db.insert(tableListDrinkJoin, {
      columnJoinListId: list.id,
      columnJoinDrinkId: drinkId,
    });
  }

  Future insertAll(ShoppingList list, Iterable<int> drinkIds) async {
    Batch batch = db.batch();

    for (final i in drinkIds) {
      batch.insert(tableListDrinkJoin, {
        columnJoinListId: list.id,
        columnJoinDrinkId: i,
      });
    }

    await batch.commit(noResult: true);
  }

  Future insertAllDrink(
      ShoppingList list, Iterable<Drink> drinks) async {
    await insertAll(list, drinks.map((c) => c.idDrink));
  }

  Future<List<Drink>> getDrinksForList(ShoppingList list) async {
    return (await db.rawQuery(
            '''SELECT * FROM $tableDrink INNER JOIN $tableListDrinkJoin ON
        $tableDrink.$columnIdDrink=$tableListDrinkJoin.$columnJoinDrinkId
        WHERE $tableListDrinkJoin.$columnJoinListId=${list.id}'''))
        .map<Drink>((map) => Drink.fromMap(map))
        .toList()
          ..sort();
  }

  Future<Set<Drink>> getDrinksForListAsSet(ShoppingList list) async {
    return (await db.rawQuery(
            '''SELECT * FROM $tableDrink INNER JOIN $tableListDrinkJoin ON
        $tableDrink.$columnIdDrink=$tableListDrinkJoin.$columnJoinDrinkId
        WHERE $tableListDrinkJoin.$columnJoinListId=${list.id}'''))
        .map<Drink>((map) => Drink.fromMap(map))
        .toSet();
  }

  Future<int> delete(ShoppingList list, int drinkId) async {
    return await db.delete(tableListDrinkJoin,
        where: "$columnJoinListId = ? and $columnJoinDrinkId = ?",
        whereArgs: [list.id, drinkId]);
  }

  Future<int> deleteList(ShoppingList list) async {
    return await db.delete(tableListDrinkJoin,
        where: "$columnJoinListId = ?",
        whereArgs: [list.id]);
  }

  Future deleteAll(ShoppingList list, Iterable<int> drinkIds) async {
    Batch batch = db.batch();

    for (final i in drinkIds) {
      batch.delete(tableListDrinkJoin,
          where: '$columnJoinListId = ? and $columnJoinDrinkId = ?',
          whereArgs: [list.id, i]);
    }

    await batch.commit(noResult: true);
  }

  Future deleteAllDrink(
      ShoppingList list, Iterable<Drink> drinks) async {
    await deleteAll(list, drinks.map((c) => c.idDrink));
  }

  Future<bool> contains(ShoppingList list, int drinkId) async {
    return Sqflite.firstIntValue(
          await db.rawQuery(
            "SELECT COUNT(*) FROM $tableListDrinkJoin WHERE ($columnJoinListId = ? AND $columnJoinDrinkId = ?)",
            [list.id, drinkId],
          ),
        ) !=
        0;
  }
}
