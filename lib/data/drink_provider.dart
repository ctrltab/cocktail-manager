/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/model/drink.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:sqflite/sqflite.dart';

class DrinkProvider {
  static final DrinkProvider _provider = new DrinkProvider._internal();

  static DrinkProvider get() => _provider;

  Database db;

  void open(Database db) {
    this.db = db;
  }

  Future onCreate(Database db, int version) async {
    String s = await rootBundle.loadString('assets/drinks.sql');
    var b = db.batch();
    s.split(';\n').forEach((str) {
      if (str.isNotEmpty) b.execute(str);
    });
    return b.commit(noResult: true);
  }

  Future<Drink> getDrink(int idDrink) async {
    List<Map> maps = await db.query(tableDrink,
        where: "$columnIdDrink = ?", whereArgs: [idDrink]);
    if (maps.length > 0) {
      return Drink.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Drink>> getDrinks() async {
    List<Map> list = await db.rawQuery('SELECT * FROM $tableDrink');

    return list
        .map<Drink>((map) => Drink.fromMap(map))
        .toList(growable: false);
  }

  Future<List<Drink>> getDrinksBySearchByName(String criteria) async {
    List<Map> list = await db.rawQuery(
        'SELECT * FROM $tableDrink WHERE $columnStrDrink LIKE \'$criteria%\'');

    return list
        .map<Drink>((map) => Drink.fromMap(map))
        .toList(growable: false)
          ..sort();
  }

  Future<List<Drink>> getDrinksForCategory(String category) async {
    return _getDrinksForColumn(columnStrCategory, category);
  }

  Future<List<String>> getCategories() async {
    return await _getColumnDistinct(columnStrCategory);
  }

  Future<List<Drink>> getDrinksForGlass(String glass) async {
    return _getDrinksForColumn(columnStrGlass, glass);
  }

  Future<List<String>> getGlasses() async {
    return await _getColumnDistinct(columnStrGlass);
  }

  Future<List<Drink>> getDrinksForAlcoholic(String alcoholic) async {
    return _getDrinksForColumn(columnStrAlcoholic, alcoholic);
  }

  Future<List<String>> getAlcoholicTypes() async {
    return await _getColumnDistinct(columnStrAlcoholic);
  }

  Future<List<Drink>> getDrinksForIBA(String iba) async {
    return _getDrinksForColumn(columnStrIBA, iba);
  }

  Future<List<String>> getIBA() async {
    return await _getColumnDistinct(columnStrIBA);
  }

  Future<List<Drink>> getDrinksForIngredient(String ingredient) async {
    return _getDrinksForColumns([
      columnStrIngredient1,
      columnStrIngredient2,
      columnStrIngredient3,
      columnStrIngredient4,
      columnStrIngredient5,
      columnStrIngredient6,
      columnStrIngredient7,
      columnStrIngredient8,
      columnStrIngredient9,
      columnStrIngredient10,
      columnStrIngredient11,
      columnStrIngredient12,
      columnStrIngredient13,
      columnStrIngredient14,
      columnStrIngredient15,
    ], ingredient);
  }

  Future<List<String>> getIngredients() async {
    return await _getColumnsDistinct([
      columnStrIngredient1,
      columnStrIngredient2,
      columnStrIngredient3,
      columnStrIngredient4,
      columnStrIngredient5,
      columnStrIngredient6,
      columnStrIngredient7,
      columnStrIngredient8,
      columnStrIngredient9,
      columnStrIngredient10,
      columnStrIngredient11,
      columnStrIngredient12,
      columnStrIngredient13,
      columnStrIngredient14,
      columnStrIngredient15,
    ]);
  }

  Future<List<Drink>> _getDrinksForColumn(
      String column, String query) async {
    List<Map> list =
        await db.query(tableDrink, where: '$column = ?', whereArgs: [query]);

    return list.map<Drink>((m) => Drink.fromMap(m)).toList()
      ..sort();
  }

  Future<List<Drink>> _getDrinksForColumns(
      List<String> columns, String query) async {
    List<Map> list = await db.query(tableDrink,
        where: columns.map((s) => '$s = ?').join(' or '),
        whereArgs: List.generate(columns.length, (index) => query));

    return list.map<Drink>((m) => Drink.fromMap(m)).toList()
      ..sort();
  }

  Future<List<String>> _getColumnDistinct(String column) async {
    return _getColumnsDistinct([column]);
  }

  Future<List<String>> _getColumnsDistinct(List<String> columns) async {
    return (await db.query(tableDrink, distinct: true, columns: columns))
        .map<String>((m) {
          String res;
          columns.forEach((str) {
            if (m[str] != null) res = m[str];
          });
          return res;
        })
        .toSet()
        .toList()
          ..remove(null)
          ..sort((s1, s2) => s1.compareTo(s2));
  }

  DrinkProvider._internal();
}
