/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/data/drink_provider.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/search_query.dart';

class Repository {
  static final Repository _repo = new Repository._internal();

  static Repository get() => _repo;

  DrinkProvider cp = DrinkProvider.get();

  SearchQuery categorySearch;
  SearchQuery ingredientSearch;
  SearchQuery glassSearch;
  SearchQuery alcoholicSearch;
  SearchQuery IBASearch;
  SearchQuery nameSearch;

  List<SearchQuery> searches;

  Future<Drink> getDrink(int id) {
    return cp.getDrink(id);
  }

  Future<List<Drink>> searchByName(String criteria) async {
    return cp.getDrinksBySearchByName(criteria);
  }

  Repository._internal() {
    categorySearch =
        new SearchQuery('Category', cp.getDrinksForCategory, cp.getCategories);
    ingredientSearch = new SearchQuery(
        'Ingredient', cp.getDrinksForIngredient, cp.getIngredients);
    glassSearch = new SearchQuery('Glass', cp.getDrinksForGlass, cp.getGlasses);
    alcoholicSearch = new SearchQuery(
        'Alcoholic', cp.getDrinksForAlcoholic, cp.getAlcoholicTypes);
    IBASearch = new SearchQuery('IBA', cp.getDrinksForIBA, cp.getIBA);
    nameSearch = new SearchQuery(
        'Name',
        cp.getDrinksBySearchByName,
        () => cp.getDrinks().then<List<String>>(
            (List<Drink> val) => val.map((Drink f) => f.strDrink).toList()));

    searches = [
      categorySearch,
      ingredientSearch,
      IBASearch,
      glassSearch,
      alcoholicSearch,
      nameSearch,
    ];
  }
}
