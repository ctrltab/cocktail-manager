/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/model/shopping_list.dart';
import 'package:sqflite/sqflite.dart';

class DrinkListProvider {
  static final DrinkListProvider _provider = new DrinkListProvider._internal();

  DrinkListProvider._internal();

  static DrinkListProvider get() => _provider;

  Database db;

  void open(Database db) {
    this.db = db;
  }

  Future onCreate(Database db, int version) async {
    db.execute('''create table $tableList (
    $columnIdList integer primary key autoincrement,
    $columnStrTitle text not null,
    $columnStrDescription text not null)''');

    await db.insert(tableList, ShoppingLists.fav.toMap());
  }

  Future insert(ShoppingList list) async {
    list.id = await db.insert(tableList, list.toMap());
  }

  Future<ShoppingList> getList(int idList) async {
    List<Map> maps = await db
        .query(tableList, where: "$columnIdList = ?", whereArgs: [idList]);
    if (maps.length > 0) {
      return ShoppingList.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete(int idList) async {
    return await db
        .delete(tableList, where: "$columnIdList = ?", whereArgs: [idList]);
  }

  Future<List<ShoppingList>> getShoppingLists() async {
    List<Map> list = await db.rawQuery('SELECT * FROM $tableList');

    return list
        .map<ShoppingList>((map) => ShoppingList.fromMap(map))
        .toList(growable: false);
  }
}
