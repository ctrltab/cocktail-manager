/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:async';

import 'package:drink_manager/data/drink_list_provider.dart';
import 'package:drink_manager/data/list_drink_join_provider.dart';
import 'package:drink_manager/main.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/shopping_list.dart';
import 'package:drink_manager/pages/home.dart';
import 'package:drink_manager/widgets/drink_preview.dart';
import 'package:flutter/material.dart';

class DrinkListPage extends StatefulWidget {
  @override
  _DrinkListPageState createState() => new _DrinkListPageState();
}

class _DrinkListPageState extends State<DrinkListPage> with TickerProviderStateMixin<DrinkListPage> {
  Map<ShoppingList, Set<Drink>> list = {};

  StreamSubscription<ListChangedEvent> stream;

  Set<Drink> selected = Set();
  Set<Drink> copy = Set();

  bool searching = false;

  TabController tabController;
  TextEditingController textEditingController;

  @override
  void initState() {
    super.initState();

    DrinkListProvider.get().getShoppingLists().then((lists) {
      lists.forEach((shop) {
        ListDrinkJoinProvider.get().getDrinksForListAsSet(shop).then((drinks) {
          this.list[shop] = drinks;
          if (this.list.length == lists.length) {
            setState(() {
              tabController = TabController(length: list.length, vsync: this);
              tabController.addListener(() {
                setState(() {});
              });
            });
          }
        });
      });
    });

    stream = MyApp.bus.on<ListChangedEvent>().listen(
      (e) async {
        setState(
          () {
            if (e.change == ListChange.Added) {
              list[e.shoppingList].addAll(e.drinks);
            } else if (e.change == ListChange.Set) {
              list[e.shoppingList] = e.drinks;
              tabController = TabController(length: list.length, vsync: this, initialIndex: tabController.index);
            } else if (e.change == ListChange.Removed) {
              list[e.shoppingList].removeAll(e.drinks);
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                      '${e.drinks.length == 1 ? e.drinks.first.strDrink : e.drinks.length} removed from ${e.shoppingList.title}'),
                  action: SnackBarAction(
                    label: 'Undo',
                    onPressed: () {
                      MyApp.bus.fire(
                        ListChangedEvent(shoppingList: e.shoppingList, change: ListChange.Added, drinks: e.drinks),
                      );
                    },
                  ),
                ),
              );
            }
          },
        );
      },
    );

    textEditingController = TextEditingController();
    textEditingController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    stream.cancel();
    tabController.dispose();
    textEditingController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext contextBuild) {
    if (list.length == 0) return Text('asdasd');


    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          searching
              ? SliverAppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            iconTheme: Theme.of(context).iconTheme,
            textTheme: Theme.of(context).textTheme,
            pinned: true,
            expandedHeight: 104.0,
            forceElevated: innerBoxIsScrolled,
            title: TextField(
              controller: textEditingController,
              autofocus: true,
              maxLines: 1,
              decoration: InputDecoration.collapsed(hintText: 'Search', border: UnderlineInputBorder()),
            ),
            // floating: true,
          )
              : selected.isNotEmpty
              ? SliverAppBar(
            backgroundColor: Theme.of(context).backgroundColor,
            iconTheme: Theme.of(context).iconTheme,
            textTheme: Theme.of(context).textTheme,
            pinned: true,
            expandedHeight: 104.0,
            forceElevated: innerBoxIsScrolled,
            title: Text('Selected ${selected.length}'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.content_copy),
                tooltip: 'Copy',
                onPressed: () {
                  setState(() {
                    copy = selected;
                  });
                  Navigator.pop(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                tooltip: 'Remove',
                onPressed: () {
                  setState(() {
                    ShoppingList shop = list.keys.elementAt(tabController.index);
                    ListDrinkJoinProvider.get().deleteAllDrink(shop, selected);
                    MyApp.bus.fire(
                        ListChangedEvent(change: ListChange.Removed, shoppingList: shop, drinks: selected));
                    Navigator.pop(context);
                  });
                },
              ),
            ],
            // floating: true,
          )
              : SliverAppBar(
            floating: true,
            pinned: true,
            snap: true,
            forceElevated: innerBoxIsScrolled,
            title: const Text('List'),
            bottom: TabBar(
              controller: tabController,
              isScrollable: true,
              tabs: list.keys.map((list) => Tab(text: list.title)).toList(),
            ),
            actions: <Widget>[
              copy.isNotEmpty
                  ? IconButton(
                icon: Icon(Icons.content_paste),
                tooltip: 'Paste',
                onPressed: () {
                  ShoppingList shop = list.keys.elementAt(tabController.index);
                  setState(() {
                    list[shop].addAll(copy);
                  });
                  ListDrinkJoinProvider.get().insertAllDrink(shop, copy);
                },
              )
                  : null,
              IconButton(
                icon: Icon(Icons.shopping_basket),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.delete),
                tooltip: 'Delete',
                onPressed: list.keys.elementAt(tabController.index) == ShoppingLists.fav
                    ? null
                    : () {
                  ShoppingList shop = list.keys.elementAt(tabController.index);
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        title: Text('Confirm deletion of ${shop.title}'),
                        actions: <Widget>[
                          FlatButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('Cancel'),
                          ),
                          FlatButton(
                            onPressed: () {
                              ListDrinkJoinProvider.get().deleteList(shop);
                              DrinkListProvider.get().delete(shop.id);
                              Navigator.of(context).pop();
                              setState(() {
                                list.remove(shop);
                                tabController = TabController(
                                    length: list.length,
                                    vsync: this,
                                    initialIndex: tabController.index - 1);
                              });
                            },
                            child: Text('Ok'),
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.search),
                tooltip: 'Search',
                onPressed: () {
                  setState(() {
                    ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: () {
                      setState(() {
                        searching = false;
                        textEditingController.text = '';
                      });
                    }));

                    setState(() {
                      searching = true;
                    });
                    // searching = true;
                  });
                },
              ),
            ]..removeWhere((w) => w == null),
            // floating: true,
          ),
        ];
      },
      body: TabBarView(
        controller: tabController,
        children: list.keys.map((key) {
          return SafeArea(
            top: false,
            bottom: false,
            child: Builder(
              builder: (BuildContext context) {
                List<Drink> ing = list[key].toList()..sort();

                bool noDrinks = ing.isEmpty;

                ing.removeWhere((d) => !d.strDrink.toLowerCase().contains(textEditingController.text));

                Widget sliver = noDrinks
                    ? SliverToBoxAdapter(
                  child: NoDrinksInList(key),
                )
                    : SliverPadding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate(
                          (context, index) => DrinkPreview(
                        ing[index],
                        onLongPress: selected.isEmpty && !searching
                            ? () {
                          ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: () {
                            setState(() {
                              selected = Set();
                            });
                          }));
                          setState(() {
                            selected.add(ing[index]);
                          });
                        }
                            : null,
                        onTap: selected.isNotEmpty && !searching
                            ? () {
                          setState(() {
                            selected.contains(ing[index])
                                ? selected.remove(ing[index])
                                : selected.add(ing[index]);
                            if (selected.isEmpty) {
                              Navigator.pop(context);
                            }
                          });
                        }
                            : null,
                        selected: selected.contains(ing[index]),
                      ),
                      childCount: ing.length,
                    ),
                  ),
                );

                return CustomScrollView(
                  key: PageStorageKey<String>(key.title),
                  slivers: <Widget>[
                    sliver,
                  ],
                );
              },
            ),
          );
        }).toList(),
      ),
    );
    /* return SearchBody(
      list: Future(() => ingredients.toList()),
      builder: (name) => new IngredientResult(name),
      getValue: (name) => name,
    );*/
  }
}
