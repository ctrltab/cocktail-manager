/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:collection';

import 'package:drink_manager/data/repository.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/widgets/drink_preview.dart';
import 'package:drink_manager/widgets/ingredient_preview.dart';
import 'package:drink_manager/widgets/search_body.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrowsePage extends StatefulWidget {
  @override
  _BrowsePageState createState() => new _BrowsePageState();
}

class _BrowsePageState extends State<BrowsePage> {
  HashMap<String, String> selected = new HashMap();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text('Browse'),
      ),
      body: new ListView(
        children: //<List<Widget>>[
            Repository.get()
                .searches
                .map(
                  (search) => Card(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).push(
                              new MaterialPageRoute(
                                builder: (context) {
                                  if (search == Repository.get().nameSearch) {
                                    return SearchBody<Drink>(
                                      //FIXME: This is not optimal
                                      title: 'Name',
                                      list: search.getDrinks(''),
                                      builder: (c) => new DrinkPreview(c),
                                      getValue: (c) => c.strDrink,
                                    );
                                  } else {
                                    return SearchBody<String>(
                                      title: search.name,
                                      list: search.getQueries(),
                                      builder: (q) {
                                        void onTap() {
                                          Navigator.of(context).push(
                                            new MaterialPageRoute(
                                              builder: (context) {
                                                return SearchBody<Drink>(
                                                  title: q,
                                                  list: search.getDrinks(q),
                                                  builder: (c) =>
                                                      new DrinkPreview(c),
                                                  getValue: (c) => c.strDrink,
                                                );
                                              },
                                            ),
                                          );
                                        }

                                        if (search ==
                                            Repository.get().ingredientSearch) {
                                          return new IngredientPreview(
                                            q,
                                            onTap: onTap,
                                          );
                                        } else {
                                          return ListTile(
                                            title: Text(q),
                                            onTap: onTap,
                                          );
                                        }
                                      },
                                      /* onSelect: (q) {
                                    Navigator.of(context).push(
                                      new MaterialPageRoute(
                                        builder: (context) {
                                          return new SearchBody<Cocktail>(
                                            title: q,
                                            list: search.getCocktails(q),
                                            builder: (c) =>
                                                new CocktailPreview(c),
                                            onSelect: (c) {
                                              CocktailShowcase.showCocktail(
                                                  c, context);
                                            },
                                            getValue: (c) => c.strDrink,
                                          );
                                        },
                                      ),
                                    );
                                  },*/
                                      /*   onLongPress: (q) async {
                                    if ((await Repository
                                            .get()
                                            .ingredientSearch
                                            .getQueries())
                                        .contains(q)) {}
                                  },*/
                                      getValue: (element) => element,
                                    );
                                  }
                                },
                              ),
                            );
                          },
                          child:
                              /*Column(
                          children: <Widget>[*/
                              new Container(
                            child: new Text(search.name),
                            height: 80.0,
                            alignment: Alignment.center,
                          ),
                          /*new Row(
                              children: <Widget>[
                                new Expanded(
                                  child: new FutureBuilder(
                                    future: search.getQueries(),
                                    builder: (_, snapshot) {
                                      if (snapshot.hasData) {
                                        return new DropdownButton<String>(
                                          value: selected[search.name],
                                          items: snapshot.data
                                              .map<DropdownMenuItem<String>>(
                                                  (str) {
                                            return new DropdownMenuItem<String>(
                                              child: new Text(str),
                                              value: str,
                                            );
                                          }).toList(),
                                          onChanged: (str) {
                                            setState(() {
                                              selected[search.name] = str;
                                            });
                                          },
                                        );
                                      } else
                                        return CircularProgressIndicator();
                                    },
                                  ),
                                ),
                                new IconButton(
                                  icon: new Icon(Icons.clear),
                                  onPressed: () {
                                    setState(
                                      () {
                                        selected[search.name] = null;
                                      },
                                    );
                                  },
                                ),
                              ],
                            )*/
                          //],
                          //),
                        ),
                      ),
                )
                .toList(),
        // [new RaisedButton(child: new Text('Go!'), onPressed: () {})],
        //].expand((f) => f).toList(),
      ),
    );
  }
}
