/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:drink_manager/data/list_drink_join_provider.dart';
import 'package:drink_manager/data/drink_list_provider.dart';
import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/shopping_list.dart';
import 'package:drink_manager/widgets/drink_showcase.dart';
import 'package:drink_manager/widgets/ingredient_preview.dart';
import 'package:drink_manager/widgets/shopping_list_dialog.dart';
import 'package:flutter/material.dart';

class ShoppingListPage extends StatefulWidget {
  @override
  _ShoppingListPageState createState() => new _ShoppingListPageState();
}

class _ShoppingListPageState extends State<ShoppingListPage>
    with TickerProviderStateMixin<ShoppingListPage> {
  Map<ShoppingList, List<Drink>> list = {};
  Set<String> dismissed = Set();

  bool fullList = true;

  @override
  void initState() {
    DrinkListProvider.get().getShoppingLists().then((lists) {
      lists.forEach((shop) {
        ListDrinkJoinProvider.get()
            .getDrinksForList(shop)
            .then((drinks) {
          this.list[shop] = drinks;
          if (this.list.keys.length == lists.length) {
            setState(() {});
          }
        });
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TabController controller =
        new TabController(length: list.length, vsync: this);

    return new Scaffold(
      appBar: AppBar(
        title: Text('Shopping List'),
        bottom: TabBar(
          isScrollable: true,
          controller: controller,
          tabs: list.keys.map((list) => Tab(text: list.title)).toList(),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              showDialog(
                  context: context, builder: (_) => NewShoppingListDialog());
            },
          ),
          IconButton(
            icon: Icon(Icons.remove_red_eye),
            onPressed: () {
              setState(() {
                fullList = !fullList;
              });
            },
          )
        ],
      ),
      body: TabBarView(
        controller: controller,
        children: list.values.map((cs) {
          if (fullList) {
            return _FullList(
                cs
                    .expand((drink) => drink.ingredients.keys)
                    .toSet()
                    .toList()
                      ..sort((s1, s2) => s1.compareTo(s2)),
                this);
          } else {
            return _DrinkList(
                cs..sort(), this);
          }
        }).toList(),
      ),
    );

    /* return SearchBody(
      list: Future(() => ingredients.toList()),
      builder: (name) => new IngredientResult(name),
      getValue: (name) => name,
    );*/
  }

  Widget getIngredient(String s) {
    return IngredientPreview(
      s,
      trailing: Checkbox(
          value: dismissed.contains(s),
          onChanged: (val) {
            setState(() {
              if (val)
                dismissed.add(s);
              else
                dismissed.remove(s);
            });
          }),
    );
  }
}

class _FullList extends StatelessWidget {
  final List<String> ingredients;
  final _ShoppingListPageState parent;

  _FullList(this.ingredients, this.parent);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: ingredients.length,
      itemBuilder: (context, index) => parent.getIngredient(ingredients[index]),
      separatorBuilder: (BuildContext context, int index) => Divider(
            height: 0.0,
            indent: 100.0,
          ),
    );
  }
}

class _DrinkList extends StatelessWidget {
  final List<Drink> drinks;
  final _ShoppingListPageState parent;

  _DrinkList(this.drinks, this.parent);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: drinks.length,
      itemBuilder: (context, index) {
        Drink c = drinks[index];
        return InkWell(
          onTap: () {
            DrinkShowcase.showDrink(c, context);
          },
          child: Card(
            child: Stack(
              alignment: Alignment.centerLeft,
              children: <Widget>[
                Positioned.fill(
                    child: Hero(
                  tag: 'image:${c.idDrink}',
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: CachedNetworkImageProvider(c.strDrinkThumb),
                      ),
                    ),
                    child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context).cardColor.withAlpha(80)),
                      ),
                    ),
                  ),
                )),
                Column(
                  children: [
                    [
                      ListTile(
                        title: Text(
                          c.strDrink,
                          textAlign: TextAlign.start,
                        ),
                      )
                    ],
                    c.ingredients.keys
                        .map((s) => parent.getIngredient(s))
                        .toList(),
                  ].expand<Widget>((f) => f).toList(),
                ),
              ],
            ),
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => Divider(
            height: 8.0,
          ),
    );
  }
}
