/*
 * Copyright 2018 Markus Toran.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import 'package:drink_manager/model/drink.dart';
import 'package:drink_manager/model/shopping_list.dart';
import 'package:drink_manager/pages/browse_page.dart';
import 'package:drink_manager/pages/drink_list_page.dart';
import 'package:drink_manager/pages/shopping_list_page.dart';
import 'package:drink_manager/widgets/drink_preview.dart';
import 'package:drink_manager/widgets/shopping_list_dialog.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:material_search/material_search.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  MyHomePageState createState() => new MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    Widget body;
    Widget fab;

    switch (index) {
      case 0:
        DrinkListPage page = DrinkListPage();
        body = page;
        fab = FloatingActionButton(
          onPressed: () {
            showDialog(
                context: context, builder: (_) => NewShoppingListDialog());
          },
          child: Icon(Icons.add),
        );
        break;
      case 1:
        body = BrowsePage();
        break;
      case 2:
        body = ShoppingListPage();
    }

    return new Scaffold(
      /*appBar: new AppBar(
        title: new Text(widget.title),
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.search),
              tooltip: 'Search',
              onPressed: _search)
        ],
      ),*/
      floatingActionButton: fab,
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      body: body,
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.favorite),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.search),
              title: Text('Browse')),
          BottomNavigationBarItem(
              backgroundColor: Theme.of(context).primaryColor,
              icon: Icon(Icons.shopping_cart),
              title: Text('Shopping List')),
        ],
        onTap: (index) {
          if(Navigator.canPop(context)) Navigator.pop(context);
          setState(() {
            this.index = index;
          });
        },
        currentIndex: index,
      ),
/*      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new ListTile(
              leading: new Icon(Icons.home),
              title: new Text('Overview'),
            ),
            new ListTile(
              leading: new Icon(Icons.search),
              title: new Text('Browse'),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(
                  new MaterialPageRoute(
                    builder: (b) {
                      return new BrowsePage();
                    },
                  ),
                );
                //   Navigator.of(context).pop();
              },
            ),
            new ListTile(
              leading: new Icon(Icons.shopping_cart),
              title: new Text('Shopping List'),
              onTap: () {
                Navigator.of(context).push(
                  new MaterialPageRoute(
                    builder: (b) {
                      return new ShoppingListPage();
                    },
                  ),
                );
                //   Navigator.of(context).pop();
              },
            ),
            new ListTile(
              leading: new Icon(Icons.view_list),
              title: new Text('List'),
              onTap: () {
                Navigator.of(context).push(
                  new MaterialPageRoute(
                    builder: (b) {
                      return new CocktailListPage();
                    },
                  ),
                );
                //   Navigator.of(context).pop();
              },
            ),
            new Divider(),
            new AboutListTile(
              aboutBoxChildren: <Widget>[
                new Text(
                    'Support https://www.thecocktaildb.com if you want to help \n Icon designed by Freepik from Flaticon licenced under Creative Commons BY 3.0')
              ],
            ),
          ],
        ),
      ),*/
    );
  }
}

class CustomSearchResult extends MaterialSearchResult<Drink> {
  final value;

  const CustomSearchResult({
    Key key,
    this.value,
  }) : super(key: key, value: value);

  @override
  Widget build(BuildContext context) {
    return DrinkPreview(value);
  }
}

class NoDrinksInList extends StatelessWidget {
  final ShoppingList list;

  NoDrinksInList(this.list);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned.fill(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text('Drinks will be displayed here.'),
                  Text('You have not yet added Drinks to ${list.title}.'),
                  Text(' Add some!'),
                ],
              ),
            ),
          ),
          Icon(
            Icons.sentiment_very_satisfied,
            size: 350.0,
            color: Theme.of(context).hintColor.withAlpha(20),
          )
        ],
      ),
    );
  }
}
