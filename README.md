# Cocktail manager

A flutter app aggregating cocktails and other drinks.
See my [website](https://toranm.drunkensailor.org/projects/cocktailmanager/) for more details.

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
